//+build mage

package main

import (
	"github.com/magefile/mage/sh"
)

func Run() error {
	// if err := sh.Run("go", "mod", "download"); err != nil {
	// 	return err
	// }
	if err := sh.Run("go", "build", "-o", "./bin/backend", "./cmd/backend/main.go"); err != nil {
		return err
	}
	return sh.Run("./bin/backend")
}


// go get github.com/swaggo/swag/cmd/swag
// cd "cmd/$1" && swag init -g docs.go -d . --parseDependency=true --parseInternal=true --parseDepth=3