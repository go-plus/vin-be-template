package cfg

import (
	"fmt"
	"github.com/spf13/viper"
	"log"
)

func SetupConfig() {
	viper.AddConfigPath("./static")
	env := viper.GetString("ENVIRONMENT")

	fmt.Printf("CURRENT ENV: %s", env)

	viper.SetConfigName("config.default") // config.toml
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Fatalf("Config file not found")
		} else {
			log.Fatalf("Errorf reading conf file %s", err.Error())
		}
	}
}
