package cfg

const (
	ConfigKeyEnvironment = "ENVIRONMENT"
	// EnvironmentDev dev environment
	EnvironmentDev = "DEV"
	// EnvironmentPro prod environment
	EnvironmentPro = "PROD"
	// EnvironmentQC qc environment
	EnvironmentQC = "QC"
	// EnvironmentLocal local environment
	EnvironmentLocal = "LOCAL"
	// EnvironmentTest testing environment
	EnvironmentTest = "TEST"
	// EnvironmentUAT uat environment
	EnvironmentUAT = "UAT"
)
