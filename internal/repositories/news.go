package repositories

import (
	"log"
	"vin-be-template/internal/db"
	"vin-be-template/internal/errors"
	"vin-be-template/internal/models"
)

type INewRepository interface {
	FindByID(id int64) (*models.New, error)
	FindBySaleIDAndID(saleID int64, id int64) (*models.New, error)
	FindAllBySaleID(saleID int64, displayType string) ([]*models.New, error)
}

type newRepo struct {
	db *db.DB
}

func NewNewRepository(db *db.DB) INewRepository {
	return &newRepo{
		db: db,
	}
}

func (_this *newRepo) FindByID(id int64) (*models.New, error) {
	var item models.New

	dbTnx := _this.db.DB().Table(models.TableNameNew)

	err := dbTnx.Where("id = ?", id).First(&item).Error
	if err != nil {
		return nil, err
	}

	return &item, nil
}

func (_this *newRepo) FindBySaleIDAndID(saleID int64, id int64) (*models.New, error) {
	var results []*models.New
	sql := ` SELECT n.*
             FROM news n
				LEFT JOIN groups_news gn ON gn.news_id = n.id
 				LEFT JOIN groups g ON gn.group_id = g.id
				LEFT JOIN groups_sales gs ON gs.group_id = g.id
			 WHERE (gs.sale_id = ? AND n.id = ?)
					OR (n.type = 'ALL_SALES'  AND n.id = ?)`
	err := _this.db.DB().Raw(sql, saleID, id, id).Scan(&results).Error
	if err != nil {
		log.Println(err.Error())
	}
	if len(results) == 0 {
		return nil, errors.NewCusErr(errors.ErrNewsNotFound)
	}

	return results[0], nil
}

func (_this *newRepo) FindAllBySaleID(saleID int64, displayType string) ([]*models.New, error) {
	var results []*models.New
	sql := ` SELECT n.*
             FROM news n
				LEFT JOIN groups_news gn ON gn.news_id = n.id AND gn.deleted_at IS NULL
 				LEFT JOIN groups g ON gn.group_id = g.id AND g.deleted_at IS NULL
				LEFT JOIN groups_sales gs ON gs.group_id = g.id AND gs.deleted_at IS NULL
				where (gs.sale_id = ? OR n.type = 'ALL_SALES' )
                      AND ( n.start_time IS NULL 
							OR (n.start_time <= CURRENT_TIMESTAMP
									AND (n.end_time IS NULL OR n.end_time >= CURRENT_TIMESTAMP)
							)
						)
   					  AND n.display_type = ?
                      AND n.status = 'published'
                      AND n.deleted_at IS NULL
			 ORDER BY n.updated_at DESC`
	err := _this.db.DB().Raw(sql, saleID, displayType).Scan(&results).Error
	if err != nil {
		return nil, err
	}
	return results, nil
}
