// Package repositories a Auth repository.
package repositories

import (
	"go.uber.org/dig"
	"vin-be-template/internal/db"
	"vin-be-template/internal/models"
)

// AuthRepository represents a repository for managing test of merchants.
type AuthRepository interface {
	FindByPhoneNumber(phoneNumber string) (*models.Auth, error)
	FindByUsernameAndType(username string, loginType string) (*models.Auth, error)
	FindByID(id int64) (*models.Auth, error)
	FindBySaleID(saleId int64) (*models.Auth, error)
	Create(auth *models.Auth) error
}

type authRepository struct {
	db db.DB
}

type authRepositoryParams struct {
	dig.In
	DB db.DB
}

// NewAuthRepository create a new instance of Auth Repository.
func NewAuthRepository(params authRepositoryParams) AuthRepository {
	return &authRepository{
		db: params.DB,
	}
}

func (_this *authRepository) Create(auth *models.Auth) error {
	return _this.db.DB().Create(&auth).Scan(&auth).Error
}

func (_this *authRepository) FindByUsernameAndType(username string, loginType string) (*models.Auth, error) {
	var auth models.Auth
	err := _this.db.DB().Model(models.Auth{}).
		Preload("SaleInfo").
		Where("username = ?", username).
		Where("type = ?", loginType).
		First(&auth).Error
	if err != nil {
		return nil, err
	}
	return &auth, nil
}

func (_this *authRepository) FindByID(id int64) (*models.Auth, error) {
	var auth models.Auth
	err := _this.db.DB().Model(models.Auth{}).
		Preload("SaleInfo").
		Where("id = ?", id).First(&auth).Error
	if err != nil {
		return nil, err
	}
	return &auth, nil
}

func (_this *authRepository) FindBySaleID(id int64) (*models.Auth, error) {
	var auth models.Auth
	err := _this.db.DB().Model(models.Auth{}).Where("sale_id = ?", id).First(&auth).Error
	if err != nil {
		return nil, err
	}
	return &auth, nil
}

func (_this *authRepository) FindByPhoneNumber(phoneNumber string) (*models.Auth, error) {
	var auth models.Auth
	err := _this.db.DB().Model(models.Auth{}).
		Joins("left join sales on auths.sale_id = sales.id").
		Where("sales.phone = ?", phoneNumber).
		Preload("SaleInfo").
		First(&auth).Error
	if err != nil {
		return nil, err
	}
	return &auth, nil
}
