package handlers

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
	"net/http"
	"vin-be-template/internal/services"
)

// HealthCheckHandler handles all request of health check module.
type HealthCheckHandler struct {
	BaseHandler
	healthCheckService services.HealthCheckService
}

// NewHealthCheckHandlerParams contains all dependencies of HealthCheckHandler.
type NewHealthCheckHandlerParams struct {
	dig.In
	BaseHandler        BaseHandler
	HealthCheckService services.HealthCheckService
}

// NewHealthCheckHandler returns a new instance of HealthCheckHandler.
func NewHealthCheckHandler(params NewHealthCheckHandlerParams) *HealthCheckHandler {
	return &HealthCheckHandler{
		BaseHandler:        params.BaseHandler,
		healthCheckService: params.HealthCheckService,
	}
}

// HealthCheck handles health check API.
// @Summary HealthCheckHandler - HealthCheck
// @Description Handles health check API
// @Tags HealthCheck
// @Accept  json
// @Produce json
// @Success 200 {object} dtos.HealthCheckResponse
// @Router /health-check [GET]
func (_this *HealthCheckHandler) HealthCheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		data, err := _this.healthCheckService.HealthCheck(c)
		_this.HandleResponse(c, data, err)
		c.Next()
	}
}

func (_this *HealthCheckHandler) ShortURL() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "https://vinshop.page.link/?link=https://vinshop.onelink.me/daJo?pid%3Dnowhere%26c%3Dnowhere%26af_dp%3Doneb://platform/home_gt?tab%253D1&apn=com.vingroup.VinIDMerchantApp&isi=1477309624&ibi=com.vingroup.VinIDMerchantApp&st=%C4%90%E1%BA%B7t+h%C3%A0ng+tr%C3%AAn+VinShop&sd=VinShop+l%C3%A0+%E1%BB%A9ng+d%E1%BB%A5ng+di+%C4%91%E1%BB%99ng+ra+%C4%91%E1%BB%9Di+nh%E1%BA%B1m+h%E1%BB%97+tr%E1%BB%A3+c%C3%A1c+ch%E1%BB%A7+c%E1%BB%ADa+h%C3%A0ng+t%E1%BA%A1p+ho%C3%A1+ti%E1%BA%BFp+c%E1%BA%ADn+ngu%E1%BB%93n+h%C3%A0ng+phong+ph%C3%BA,+gi%C3%A1+c%E1%BA%A3+minh+b%E1%BA%A1ch+c%C3%B9ng+c%C3%A1c+ch%C6%B0%C6%A1ng+tr%C3%ACnh+%C6%B0u+%C4%91%C3%A3i+h%E1%BA%A5p+d%E1%BA%ABn.+Ch%E1%BB%A7+t%E1%BA%A1p+h%C3%B3a+c%C3%B3+th%E1%BB%83+d%C3%B9ng+VinShop+%C4%91%E1%BB%83+%C4%91%E1%BA%B7t+m%E1%BB%99t+l%C3%BAc+nhi%E1%BB%81u+m%E1%BA%B7t+h%C3%A0ng+v%C3%A0+%C4%91%C6%B0%E1%BB%A3c+giao+h%C3%A0ng+ngay+ng%C3%A0y+h%C3%B4m+sau.&si=https://cdn-www.vinid.net/2b80831e-hco-7656-ob-jul-thumbnail-1907-1920x1080-1.jpg")
	}
}
