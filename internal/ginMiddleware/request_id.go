package ginMiddleware

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"vin-be-template/internal/dtos"
)

// RequestIDLoggingMiddleware RequestIDLoggingMiddleware
func RequestIDLoggingMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		var reqID string
		reqID = c.GetHeader(dtos.HeaderXRequestID)
		if reqID == "" {
			reqID = uuid.New().String()
			c.Header(dtos.HeaderXRequestID, reqID)
		}

		c.Set(dtos.GinContextHeaderRequestID, reqID)
		c.Next()
	}
}
