package basicauth

import (
	b64 "encoding/base64"
	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
	"strings"
	"vin-be-template/internal/dtos"
	"vin-be-template/internal/errors"
	"vin-be-template/internal/ginLogger"
	"vin-be-template/internal/handlers"
)

// BasicAuthMiddleware a basic test middleware
type Middleware struct {
	BaseHandler handlers.BaseHandler
}

type middlewareParams struct {
	dig.In
	BaseHandler handlers.BaseHandler
}

// NewBasicAuthMiddleware return a new instance of BasicAuthMiddleware.
func NewBasicAuthMiddleware(params middlewareParams) *Middleware {
	return &Middleware{
		BaseHandler: params.BaseHandler,
	}
}

// Authenticate is basic authentication for app.
func (_this *Middleware) Authenticate(basicAccounts map[string]string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			basicAuthData = c.GetHeader(dtos.HeaderAuthorization)
			splits        = strings.Split(basicAuthData, " ")
		)
		if len(splits) != 2 && splits[0] != "Basic" {
			ginLogger.Gin(c).Errorf("Authorization header is invalid format")
			_this.BaseHandler.RespondError(c, errors.NewCusErr(errors.ErrCommonUnauthorized))
			c.Abort()
			return
		}
		decodedString, err := b64.StdEncoding.DecodeString(splits[1])
		if err != nil {
			ginLogger.Gin(c).Errorf("Decoding string failed: %v", err)
			_this.BaseHandler.RespondError(c, errors.NewCusErr(errors.ErrCommonUnauthorized))
			c.Abort()
			return
		}
		userAndPwd := strings.Split(string(decodedString), ":")
		if len(userAndPwd) != 2 {
			ginLogger.Gin(c).Errorf("Cannot parse username and password from")
			_this.BaseHandler.RespondError(c, errors.NewCusErr(errors.ErrCommonUnauthorized))
			c.Abort()
			return
		}
		if pwd, ok := basicAccounts[userAndPwd[0]]; !ok || pwd != userAndPwd[1] {
			ginLogger.Gin(c).Errorf("Does not match any username and password in accounts")
			_this.BaseHandler.RespondError(c, errors.NewCusErr(errors.ErrCommonUnauthorized))
			c.Abort()
			return
		}

		c.Set(dtos.GinContextBasicUsername, userAndPwd[0])
		c.Next()
	}
}
