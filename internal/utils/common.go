package utils

import (
	"bytes"
	"encoding/json"
	"github.com/fatih/structs"
	"log"
	"os"
	"reflect"
	"regexp"
)

func KindOfData(data interface{}) reflect.Kind {

	value := reflect.ValueOf(data)
	valueType := value.Kind()

	if valueType == reflect.Ptr {
		valueType = value.Elem().Kind()
	}
	return valueType
}

// Struct2JSON convert interface to json string
func Struct2JSON(o interface{}) string {
	b, err := json.Marshal(o)
	if err != nil {
		return ""
	}
	return string(b)
}

// StringInSlice StringInSlice
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func RemoveAllUnCharacters(source string) (string, error) {
	reg, err := regexp.Compile(`[^a-zA-Z0-9]+`)
	if err != nil {
		return "", err
	}
	return reg.ReplaceAllString(source, ""), nil
}

// StringInSlice StringInSlice
func Int64InSlice(a int64, list []int64) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// BoolToInt64 BoolToInt64
func BoolToInt64(b bool) int64 {
	if b {
		return 1
	}
	return 0
}

// ContainsStructFieldValueContainsStructFieldValue
func ContainsStructFieldValue(slice interface{}, fieldName string, fieldValueToCheck interface{}) bool {
	rangeOnMe := reflect.ValueOf(slice)

	for i := 0; i < rangeOnMe.Len(); i++ {
		s := rangeOnMe.Index(i)
		f := s.FieldByName(fieldName)
		if f.IsValid() {
			if f.Interface() == fieldValueToCheck {
				return true
			}
		}
	}
	return false
}

// GetEnv : get env from .env file
func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// ExitF prints error and exit program
func ExitF(format string, a ...interface{}) {
	log.Fatalf(format, a...)
	os.Exit(1)
}

const (
	empty = ""
	tab   = "\t"
)

// PrettyJSON prints object as json
func PrettyJSON(data interface{}) (string, error) {
	buffer := new(bytes.Buffer)
	encoder := json.NewEncoder(buffer)
	encoder.SetIndent(empty, tab)

	err := encoder.Encode(data)
	if err != nil {
		return empty, err
	}
	return buffer.String(), nil
}

// CalculateSkipOffset ..
func CalculateSkipOffset(page, totalRecords, pageSize int64) (offset int64, take int64, totalPages int64) {
	totalPages = totalRecords / pageSize

	if totalRecords%pageSize > 0 {
		totalPages++
	}

	if page > totalPages {
		offset = 0
		take = 0
	} else {
		offset = (page - 1) * pageSize
		take = pageSize
	}
	return offset, take, totalPages
}

func StructToMapStringInterface(data interface{}) (map[string]interface{}, error) {
	s := structs.New(data)
	return s.Map(), nil
}
