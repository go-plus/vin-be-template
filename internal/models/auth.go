package models

import "time"

// Auth represents table Auths information.
type Auth struct {
	ID        int64      `gorm:"column:id;PRIMARY_KEY;AUTO_INCREMENT" json:"id"`
	Username  string     `gorm:"column:username;type:varchar(255)" json:"username"`
	Token     string     `gorm:"column:token" json:"-"`
	SaleId    int64      `gorm:"column:sale_id" json:"sale_id"`
	Type      string     `gorm:"column:type;type:varchar(255)" json:"type"`
	Status    string     `gorm:"column:status" json:"status"`
	// SaleInfo  Sale       `gorm:"foreignKey:sale_id"`
	CreatedAt time.Time  `gorm:"column:created_at;type:datetime;NOT NULL" json:"created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at;type:datetime;NOT NULL" json:"updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at;type:datetime;NOT NULL" json:"-"`
}

// TableName returns table name of sales table.
func (Auth) TableName() string {
	return "auths"
}
