package models

import (
	"gorm.io/datatypes"
	"vin-be-template/internal/utils"
)

const TableNameNew = "news"

type (
	NewType             string
	NewDisplayType      string
	NewStatus           string
	NewAnnouncementType string
)

const (
	// NewType constants definition.
	NewTypeAllSale   NewType = "ALL_SALES"
	NewTypeGroupSale NewType = "GROUP_SALES"
	NewTypeContest   NewType = "CONTEST"

	// NewDisplayType constants definition.
	NewDisplayTypeBanner NewDisplayType = "banner"
	NewDisplayTypeNews   NewDisplayType = "news"

	// NewAnnouncementType constants definition.
	NewAnnouncementTypeImportant    NewAnnouncementType = "Important"
	NewAnnouncementTypeWarning      NewAnnouncementType = "Warning"
	NewAnnouncementTypePositiveNews NewAnnouncementType = "PositiveNews"
	NewAnnouncementTypeNeutral      NewAnnouncementType = "Neutral"

	NewsStatusPublished = "PUBLISHED"
)

type New struct {
	ID               int64            `gorm:"column:id;primary_key"`
	Type             string           `gorm:"column:type"`
	Title            string           `gorm:"column:title"`
	Content          string           `gorm:"column:content"`
	DisplayType      string           `gorm:"column:display_type"`
	UpdatedTime      *utils.Timestamp `gorm:"column:updated_time"`
	StartTime        *utils.Timestamp `gorm:"column:start_time"`
	EndTime          *utils.Timestamp `gorm:"column:end_time"`
	Status           string           `gorm:"column:status"`
	Image            string           `gorm:"column:image"`
	CTATitle         string           `gorm:"column:cta_title"`
	DeepLink         string           `gorm:"column:deep_link"`
	AnnouncementType string           `gorm:"column:announcement_type"`
	ContentImages    *datatypes.JSON  `gorm:"column:content_images"`

	CreatedBy *string          `gorm:"column:created_by"`
	UpdatedBy *string          `gorm:"column:updated_by"`
	CreatedAt utils.Timestamp  `gorm:"column:created_at"`
	UpdatedAt *utils.Timestamp `gorm:"column:updated_at"`
	DeletedAt *utils.Timestamp `gorm:"column:deleted_at"`
}

func (*New) TableName() string {
	return TableNameNew
}
