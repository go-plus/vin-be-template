package errors

/*
Example error code is 5000102:
- 500 is HTTP status code (400, 401, 403, 500, ...)
- 01 is module represents for each handler
	+ 00 for common error for all handler
	+ 01 for health check handler
- 02 is actual error code, just auto increment and start at 1
*/

var (
	// Errors of module common
	// Format: ErrCommon<ERROR_NAME> = xxx00yy
	ErrCommonInternalServer    = ErrorCode("50000001")
	ErrCommonInvalidRequest    = ErrorCode("40000001")
	ErrCommonBindRequestError  = ErrorCode("40000002")
	ErrCommonUnauthorized      = ErrorCode("40100005")
	ErrCommonExpiredToken      = ErrorCode("40100006")
	ErrAuthorizedNotPermission = ErrorCode("40100008")
	ErrCommonForbidden         = ErrorCode("40300000")
	ErrInternalGTNotFound      = ErrorCode("40400104")
	ErrInternalSaNotFound      = ErrorCode("40400206")
	ErrGTAssigned2AnotherRoute = ErrorCode("40000301")

	ErrInvalidOTP            = ErrorCode("40000103")
	ErrOverOTPLimit          = ErrorCode("42900102")
	ErrNeedNewOTP            = ErrorCode("40400105")
	ErrNoMoreAuthOrderOTP    = ErrorCode("40000104")
	ErrTooFastFromLastOTP    = ErrorCode("40000105")
	ErrDataControlTowerError = ErrorCode("40001501")

	ErrMerchantPlatformError     = ErrorCode("50001101")
	ErrMerchantPlatformSyncError = ErrorCode("50001102")
	ErrHybrisError               = ErrorCode("50001201")
	ErrSaleCoreError             = ErrorCode("50001301")
	ErrServiceControlTowerError  = ErrorCode("50001501")

	ErrNewsNotFound = ErrorCode("40400202")
	ErrSaleNotFound = ErrorCode("40400103")
	ErrTaskNotFound = ErrorCode("40400107")

	ErrGtFormatNotFound       = ErrorCode("40400308")
	ErrReportReasonNotSupport = ErrorCode("40400309")
	ErrGtDuplicateNotFound    = ErrorCode("40400310")
	ErrLocationNotFound       = ErrorCode("40400311")

	ErrMissingResolutionMessage       = ErrorCode("40001401")
	ErrInvalidResolutionMessageLength = ErrorCode("40001402")
	ErrMissingRequiredFields          = ErrorCode("40001403")
	ErrMissingAddressUpdateReason     = ErrorCode("40001404")
	ErrMissingUpdateData              = ErrorCode("40001405")
)
