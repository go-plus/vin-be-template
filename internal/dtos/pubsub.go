package dtos

import "vin-be-template/internal/utils"

type PubSubSaleTaskEvent string

const (
	PubSubOrderEventUpdated    PubSubSaleTaskEvent = "EventOrderUpdated"
	PubSubTransferEventUpdated PubSubSaleTaskEvent = "EventSalesRouteCreated"
)

type PubSubOrderInfo struct {
	Timestamp utils.Timestamp     `json:"timestamp"`
	Source    string              `json:"source,omitempty"`
	RequestID string              `json:"request_id,omitempty"`
	TraceID   string              `json:"trace_id"`
	Event     PubSubSaleTaskEvent `json:"event"`
	Data      OrderInfoDTO        `json:"data"`
}

type PubSubTransferRouteInfo struct {
	Timestamp utils.Timestamp      `json:"timestamp"`
	Source    string               `json:"source,omitempty"`
	RequestID string               `json:"request_id,omitempty"`
	TraceID   string               `json:"trace_id"`
	Event     PubSubSaleTaskEvent  `json:"event"`
	Data      TransferRouteInfoDTO `json:"data"`
}
