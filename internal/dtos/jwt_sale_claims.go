package dtos

// SaleAuthClaims contains claims of merchant site.
type SaleAuthClaims struct {
	ExpiresAt   int64               `json:"exp,omitempty"`
	RID         string              `json:"rid,omitempty"`
	ID          string              `json:"id,omitempty"`
	Sub         int64               `json:"sub,omitempty"`
	IssuedAt    int64               `json:"iat,omitempty"`
	Name        string              `json:"name,omitempty"`
	SaleID      int64               `json:"sale_id,omitempty"`
	Permissions map[string][]string `json:"permissions"`
}

// Valid uses to valid claims.
func (SaleAuthClaims) Valid() error {
	return nil
}
