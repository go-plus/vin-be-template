package dtos

const (
	GinContextUserID                        = "auth_user_id"
	GinContextUsername                      = "auth_username"
	GinContextPermissions                   = "auth_permissions"
	GinContextAuthVersion                   = "auth_version"
	GinContextHeaderRequestID               = "header_request_id"
	GinContextBasicUsername                 = "basic_username"
	GinContextAuthPermissionsWithAttributes = "auth_permissions_with_attributes"
	GinContextAuthorizedAttributes          = "authorized_attributes"
)
