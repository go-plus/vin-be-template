package dtos

// Headers definition.
const (
	HeaderAuthorization  = "Authorization"
	HeaderLang           = "X-Lang"
	HeaderXRequestID     = "X-Request-ID"
	HeaderXAPISecret     = "X-API-Secret"
	HeaderXAPIKey        = "X-API-Key"
	HeaderContextType    = "Content-Type"
	HeaderKeyCode        = "X-Key-Code"
	HeaderNonce          = "X-Nonce"
	HeaderSignature      = "X-Signature"
	HeaderTimestamp      = "X-Timestamp"
	HeaderCFConnectingIP = "CF-Connecting-IP"
	HeaderXForwardedFor  = "X-Forwarded-For"
	HeaderTrueClientIP   = "True-Client-IP"
	HeaderXAuthVersion   = "X-Auth-Version"
)

// Context key to transfer to next layer
const (
	IsMasterMerchant            = "is_master_merchant"
	ContextValueMerchantCodeKey = "x_merchant_code"
	ContextResponse             = "x-response"
	SaleID                      = "sale_id"
	ID                          = "id"
)

// Authorization method header
const (
	BearerAuth = "bearer"
	BasicAuth  = "basic"
)
