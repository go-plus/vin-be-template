package dtos

import (
	"encoding/json"
	"time"
	"vin-be-template/internal/utils"
)

type OrderInfoDTO struct {
	ID             int64            `json:"id" firestore:"id,omitempty"`
	GtID           int64            `json:"gt_id" firestore:"gt_id,omitempty"`
	Code           string           `json:"code" firestore:"code,omitempty"`
	Status         string           `json:"status" firestore:"status,omitempty"`
	SaleID         int64            `json:"sale_id" firestore:"sale_id,omitempty"`
	SaleAppStatus  string           `json:"sale_app_status" firestore:"sale_app_status,omitempty"`
	PaymentInfo    []*PaymentMethod `json:"payment_info" firestore:"payment_info,omitempty"`
	CreationTime   *utils.Timestamp `json:"creation_time" firestore:"creation_time,omitempty"`
	OrderShipments []*OrderShipment `json:"shipments" firestore:"shipments,omitempty"`
}

type RecentOrderInfoDTO struct {
	ID               int64      `json:"id" firestore:"id,omitempty"`
	GtID             int64      `json:"gt_id" firestore:"gt_id,omitempty"`
	Code             string     `json:"code" firestore:"code,omitempty"`
	Status           string     `json:"status" firestore:"status,omitempty"`
	SaleID           int64      `json:"sale_id" firestore:"sale_id,omitempty"`
	SaleAppStatus    string     `json:"sale_app_status" firestore:"sale_app_status,omitempty"`
	LastActivityTime *time.Time `json:"last_activity_time" firestore:"last_activity_time,omitempty"`
	//TotalShipmentInfo string     `json:"total_shipment_info" firestore:"total_shipment_info,omitempty"`
}

type OrderShipment struct {
	Code          string          `json:"code" firestore:"code,omitempty"`
	Status        string          `json:"status" firestore:"status,omitempty"`
	OrderProducts []*OrderProduct `json:"products" firestore:"products,omitempty"`
}

type OrderProduct struct {
	OfferCode                 string  `json:"offer_code" firestore:"offer_code,omitempty"`
	Code                      string  `json:"code" firestore:"code,omitempty"`
	Pk                        string  `json:"pk" firestore:"pk,omitempty"`
	ComboOfferCode            string  `json:"combo_offer_code" firestore:"combo_offer_code,omitempty"`
	SapCode                   string  `json:"sap_code" firestore:"sap_code,omitempty"`
	OriginalQuantity          *int64  `json:"original_quantity" firestore:"original_quantity,omitempty"`
	ConfirmedQuantity         *int64  `json:"confirmed_quantity" firestore:"confirmed_quantity,omitempty"`
	FailedPickedQuantity      *int64  `json:"failed_picked_quantity" firestore:"failed_picked_quantity,omitempty"`
	FailedDeliveryQuantity    *int64  `json:"failed_delivery_quantity" firestore:"failed_delivery_quantity,omitempty"`
	ReturnQuantity            *int64  `json:"return_quantity" firestore:"return_quantity,omitempty"`
	DeliveredQuantity         *int64  `json:"delivered_quantity" firestore:"delivered_quantity,omitempty"`
	FinalPrice                float64 `json:"final_price" firestore:"final_price,omitempty"`
	GiveAway                  bool    `json:"give_away" firestore:"give_away,omitempty"`
	OfferBasePrice            float64 `json:"offer_base_price" firestore:"offer_base_price,omitempty"`
	WarehouseCode             *string `json:"warehouse_code" firestore:"warehouse_code,omitempty"`
	WarehouseName             *string `json:"warehouse_name" firestore:"warehouse_name,omitempty"`
	MainCategoryLv0Code       *string `json:"main_category_lv0_code" firestore:"main_category_lv0_code,omitempty"`
	CommissionCategoryLv1Code *string `json:"commission_category_lv1_code" firestore:"commission_category_lv1_code,omitempty"`
	CommissionCategoryLv2Code *string `json:"commission_category_lv2_code" firestore:"commission_category_lv2_code,omitempty"`
	CommissionCategoryLv3Code *string `json:"commission_category_lv3_code" firestore:"commission_category_lv3_code,omitempty"`
	CommissionCategoryLv4Code *string `json:"commission_category_lv4_code" firestore:"commission_category_lv4_code,omitempty"`
	CommissionCategoryLv5Code *string `json:"commission_category_lv5_code" firestore:"commission_category_lv5_code,omitempty"`
	CommissionCategoryLv6Code *string `json:"commission_category_lv6_code" firestore:"commission_category_lv6_code,omitempty"`
}

type PaymentMethod struct {
	Code                string  `json:"code"`
	Name                string  `json:"name"`
	Amount              float64 `json:"amount"`
	PaymentGatewayType  string  `json:"payment_gateway_type"`
	PaymentProviderCode string  `json:"payment_provider_code"`
}

// converting the struct to String format.
func (u PaymentMethod) String() string {
	str, _ := json.Marshal(u)
	return string(str)
}
