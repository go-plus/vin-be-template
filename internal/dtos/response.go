package dtos

import (
	"vin-be-template/internal/meta"
)

// Response response custom message
type Response struct {
	Meta meta.Meta   `json:"meta"`
	Data interface{} `json:"data" swaggertype:"object"`
}

type PaginationResponse struct {
	Meta           meta.Meta       `json:"meta"`
	PaginationInfo *PaginationInfo `json:"pagination"`
	Data           interface{}     `json:"data" swaggertype:"object"`
}

type PaginationInfo struct {
	PageSize     int64 `json:"page_size"`
	PageOffset   int64 `json:"page_offset"`
	TotalRecords int64 `json:"total_records"`
	TotalPages   int64 `json:"total_pages"`
}
