package dtos

type GTSurveyDTO struct {
	Id   int64            `json:"id"`
	Form *GTSurveyFormDTO `json:"form"`
}

type GTSurveyFormDTO struct {
	Id                int64   `json:"id"`
	Name              string  `json:"name"`
	Description       *string `json:"description"`
	EffectiveTo       *int64  `json:"effective_to"`
	AllowEditResponse bool    `json:"allow_edit_response"`
	Status            string  `json:"status"`
	ActionUrl         *string `json:"action_url"`
}

type GTSurveyCount struct {
	Waiting int64 `json:"waiting"`
	Total   int64 `json:"total"`
}

type GTSurveyDirection struct {
	Title             string `json:"title"`
	Url               string `json:"url"`
	FullscreenEnabled bool   `json:"fullscreen_enabled"`
}
