package dtos

const (
	WorkerName          = "HYDRA-WORKER"
	AffectStatus        = "STATUS"
	AffectWholeTask     = "WHOLE-TASK"
	AffectPartlySubTask = "PARTLY-SUB-TASK"
)

type ListSaleTaskDbFilter struct {
	SaleID     int64
	PageSize   int64
	PageOffset int64
	Status     string
}
