package dtos

type TruckPlanOrderDTO struct {
	ID               int64   `json:"id"`
	ReferID          int64   `json:"refer_id"`
	RetailerName     string  `json:"retailer_name"`
	RetailerPhone    string  `json:"retailer_phone"`
	DeliveryAddress  string  `json:"delivery_address"`
	Quarter          string  `json:"quarter"`
	CodAmount        float64 `json:"cod_amount"`
	ExpectDeliveryAt string  `json:"expect_delivery_at"`
	Note             string  `json:"note"`
	Status           string  `json:"status"`
}
