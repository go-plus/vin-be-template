package dtos


type TransferRouteInfoDTO struct {
	ID                int64              `json:"id"`
	SaleID            int64              `json:"sale_id"`
	RouteID           int64              `json:"route_id"`
	RouteCodeRemoved  string             `json:"route_code_removed"`
	SyncKpis          bool               `json:"sync_kp_is"`
	SaleTaskExtraData PubSubExtraDataDTO `json:"sale_task_extra_data"`
}

type PubSubExtraDataDTO struct {
	FromSaleID int64 `json:"from_sale_id"`
	ToSaleID   int64 `json:"to_sale_id"`
	RouteID    int64 `json:"route_id"`
}
