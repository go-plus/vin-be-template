package dtos

import "vin-be-template/internal/meta"

// HealthCheckResponse contains response information of health check API.
type HealthCheckResponse struct {
	Meta meta.Meta `json:"meta"`
}
