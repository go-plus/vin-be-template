package sales_force

type ListCategoriesResponse struct {
	Message string `json:"message"`
	Data    []struct {
		Value    string      `json:"value"`
		ValidFor interface{} `json:"validFor"`
		Label    string      `json:"label"`
		Items    []struct {
			Value        string        `json:"value"`
			ValidFor     string        `json:"validFor"`
			Label        string        `json:"label"`
			Items        []interface{} `json:"items"`
			FieldAPIName string        `json:"fieldApiName"`
			DefaultValue string        `json:"defaultValue"`
			Active       string        `json:"active"`
		} `json:"items"`
		FieldAPIName string `json:"fieldApiName"`
		DefaultValue string `json:"defaultValue"`
		Active       string `json:"active"`
	} `json:"data"`
	Code int `json:"code"`
}

type GetListCaseResponse struct {
	Data struct {
		Records []struct {
			GtCode           string `json:"gt_code"`
			EstimatedSLADate string `json:"estimated_sla_date"`
			Subject          string `json:"subject"`
			Request          string `json:"request"`
			CaseNumber       string `json:"case_number"`
			FinalSolution    string `json:"final_solution"`
			Description      string `json:"description"`
			GroupRequest     string `json:"group_request"`
			SalesPhone       string `json:"sales_phone"`
			SalesID          string `json:"sales_id"`
			ShipmentNumber   string `json:"shipment_number"`
			DetailRequest    string `json:"detail_request"`
			GtName           string `json:"gt_name"`
			Status           struct {
				Label string `json:"label"`
				Value string `json:"value"`
			} `json:"status"`
			OrderNumber string `json:"order_number"`
			GtPhone     string `json:"gt_phone"`
			CreatedDate string `json:"created_date"`
		} `json:"records"`
		NumberRecordPage int `json:"number_record_page"`
		PageNumber       int `json:"page_number"`
		PageSize         int `json:"page_size"`
		TotalRecord      int `json:"total_record"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type GetDetailsCaseResponse struct {
	Message string `json:"message"`
	Data    struct {
		GtCode           string `json:"gt_code"`
		EstimatedSLADate string `json:"estimated_sla_date"`
		Subject          string `json:"subject"`
		Request          string `json:"request"`
		CaseNumber       string `json:"case_number"`
		FinalSolution    string `json:"final_solution"`
		Description      string `json:"description"`
		GroupRequest     string `json:"group_request"`
		SalesPhone       string `json:"sales_phone"`
		SalesID          string `json:"sales_id"`
		ShipmentNumber   string `json:"shipment_number"`
		DetailRequest    string `json:"detail_request"`
		GtName           string `json:"gt_name"`
		Status           struct {
			Label string `json:"label"`
			Value string `json:"value"`
		} `json:"status"`
		OrderNumber string  `json:"order_number"`
		GtPhone     string  `json:"gt_phone"`
		Image1      string `json:"image_1"`
		Image2      string `json:"image_2"`
		Image3      string `json:"image_3"`
		Image4      string `json:"image_4"`
		Image5      string `json:"image_5"`
		CreatedDate string  `json:"created_date"`
	} `json:"data"`
	Code int `json:"code"`
}

type CreateCaseRequest struct {
	Subject        string
	GroupRequest   string
	Request        string
	DetailRequest  string
	Description    string
	SalesPhone     string
	SalesID        string
	GtPhone        string
	GtCode         string
	OrderNumber    string
	ShipmentNumber string
	Files          string
}

type CreateCaseResponse struct {
	Message string `json:"message"`
	Data    struct {
		GtCode           string `json:"gt_code"`
		EstimatedSLADate string `json:"estimated_sla_date"`
		Subject          string `json:"subject"`
		Request          string `json:"request"`
		CaseNumber       string `json:"case_number"`
		FinalSolution    string `json:"final_solution"`
		Description      string `json:"description"`
		GroupRequest     string `json:"group_request"`
		SalesPhone       string `json:"sales_phone"`
		SalesID          string `json:"sales_id"`
		ShipmentNumber   string `json:"shipment_number"`
		DetailRequest    string `json:"detail_request"`
		GtName           string `json:"gt_name"`
		Status           struct {
			Label string `json:"label"`
			Value string `json:"value"`
		} `json:"status"`
		OrderNumber string `json:"order_number"`
		GtPhone     string `json:"gt_phone"`
		Image1      string `json:"image_1"`
		Image2      string `json:"image_2"`
		Image3      string `json:"image_3"`
		Image4      string `json:"image_4"`
		Image5      string `json:"image_5"`
	} `json:"data"`
	Code int `json:"code"`
}
