package sales_force

import (
	"github.com/spf13/viper"
	"go.uber.org/dig"

	"github.com/gin-gonic/gin"
)

// Paths constants definition.
const (
	EndpointGetListCategories = "/case_1seal_category"
	EndpointGetListCases      = "/case_1seal_list"
	EndpointGetCaseDetails    = "/case_1seal_detail"
	EndpointCreateCase        = "/case_1seal_create"
	Auth0                     = "2"
)

// Adapter handles all APIs for calling to checkout service.
type Adapter interface {
	Create(c *gin.Context, req CreateCaseRequest) (*CreateCaseResponse, error)
}

type adapter struct {
	appCfg *viper.Viper
}

type adapterParams struct {
	dig.In
	AppConfig *viper.Viper
}

// NewAdapter returns a new instance of Adapter.
func NewAdapter(params adapterParams) Adapter {
	return &adapter{
		appCfg: params.AppConfig,
	}
}

func (_this *adapter) Create(c *gin.Context, req CreateCaseRequest) (*CreateCaseResponse, error) {
	// resty
	return nil, nil
}
