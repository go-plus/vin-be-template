package providers

import (
	"vin-be-template/cmd/auth/internal/cfg"
	"vin-be-template/cmd/auth/internal/handlers"
	cfg2 "vin-be-template/internal/cfg"
	"vin-be-template/internal/ginLogger"
	commonMiddleware "vin-be-template/internal/ginMiddleware"
	"vin-be-template/internal/ginServer"
	"vin-be-template/internal/utils"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	_swaggerFiles "github.com/swaggo/files"
	_ginSwagger "github.com/swaggo/gin-swagger"
)

// setupRouter setup router.
func setupRouter(hs *handlers.Handlers) ginServer.GinRoutingFn {
	return func(router *gin.Engine) {
		router.Use(
			commonMiddleware.RequestIDLoggingMiddleware(),
			ginLogger.MiddlewareGin(AppName, zerolog.InfoLevel),
			commonMiddleware.Recovery(),
		)
		baseRoute := router.Group(viper.GetString(cfg.ConfigKeyContextPath))
		baseRoute.GET("health-check", hs.HealthCheck.HealthCheck())
		if !utils.StringInSlice(viper.GetString(cfg.ConfigKeyEnvironment), []string{
			cfg2.EnvironmentPro,
		}) {
			baseRoute.GET("swagger/*any", _ginSwagger.WrapHandler(_swaggerFiles.Handler))
		}

		v1 := baseRoute.Group("/v1")

		newsRoute := v1.Group("/news")
		{
			newsRoute.GET("", hs.NewsHandler.ListNews())
			newsRoute.GET("/:news-id", hs.NewsHandler.NewsDetails())
			newsRoute.GET("/latest", hs.NewsHandler.GetLatest())
			// localhost:9000/v1/news/latest
		}
	}
}
