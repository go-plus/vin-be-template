package providers

import (
	"go.uber.org/dig"
	"vin-be-template/cmd/auth/internal/handlers"
	"vin-be-template/cmd/auth/internal/services"
	"vin-be-template/internal/adapters/sales_force"
	cfg2 "vin-be-template/internal/cfg"
	"vin-be-template/internal/errors"
	"vin-be-template/internal/ginMiddleware/basicauth"
	"vin-be-template/internal/ginServer"
	commonHandler "vin-be-template/internal/handlers"
	"vin-be-template/internal/logger"
	"vin-be-template/internal/repositories"
	commonService "vin-be-template/internal/services"
)

const (
	AppName = "backend"
)

func init() {
	cfg2.SetupConfig()
}

// container is a global Container.
var container *dig.Container

// BuildContainer build all necessary containers.
func BuildContainer() *dig.Container {
	container = dig.New()
	{
		//_ = container.Provide(viper.New())
		_ = container.Provide(logger.NewLogger)
		_ = container.Provide(newServerConfig)
		_ = container.Provide(newErrorParserConfig)
		_ = container.Provide(newMySQLConnection)

		_ = container.Provide(repositories.NewNewRepository)

		_ = container.Provide(sales_force.NewAdapter)

		_ = container.Provide(newGinEngine)
		_ = container.Provide(errors.NewErrorParser)
		_ = container.Provide(ginServer.NewGinServer)

		_ = container.Provide(services.NewNewsService)

		_ = container.Provide(basicauth.NewBasicAuthMiddleware)
		_ = container.Provide(commonHandler.NewBaseHandler)
		_ = container.Provide(commonHandler.NewHealthCheckHandler)
		_ = container.Provide(handlers.NewHandlers)
		_ = container.Provide(commonService.NewHealthCheckService)

		_ = container.Provide(setupRouter)

		_ = container.Provide(handlers.NewNewsHandler)
	}

	return container
}

// GetContainer returns an instance of Container.
func GetContainer() *dig.Container {
	return container
}
