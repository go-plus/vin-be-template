package handlers

import (
	commonHandler "vin-be-template/internal/handlers"

	"go.uber.org/dig"
)

// Handlers contains all handlers.
type Handlers struct {
	HealthCheck *commonHandler.HealthCheckHandler
	NewsHandler *NewsHandler
}

// NewHandlersParams contains all dependencies of handlers.
type handlersParams struct {
	dig.In
	HealthCheck *commonHandler.HealthCheckHandler
	NewsHandler *NewsHandler
}

// NewHandlers returns new instance of Handlers.
func NewHandlers(params handlersParams) *Handlers {
	return &Handlers{
		HealthCheck: params.HealthCheck,
		NewsHandler: params.NewsHandler,
	}
}
