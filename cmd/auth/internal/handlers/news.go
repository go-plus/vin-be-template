package handlers

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
	"strconv"
	"strings"
	"vin-be-template/cmd/auth/internal/services"
	"vin-be-template/internal/errors"
	"vin-be-template/internal/handlers"
	"vin-be-template/internal/models"
)

type NewsHandler struct {
	handlers.BaseHandler
	newService services.INewsService
}

type newsHandlerParams struct {
	dig.In
	BaseHandler handlers.BaseHandler
	NewService  services.INewsService
}

func NewNewsHandler(params newsHandlerParams) *NewsHandler {
	return &NewsHandler{
		BaseHandler: params.BaseHandler,
		newService:  params.NewService,
	}
}

// NewsDetails Get list of home banner
// @Summary NewsHandler - ListNews
// @Tags News
// @Accept json
// @Produce json
// @Success 200 {object} dtos.ListNewsData
// @Failure 400,401,404,500 {object} meta.Error
// @Security BearerAuth
// @Router /v1/news [GET]
func (_this *NewsHandler) ListNews() gin.HandlerFunc {
	return func(c *gin.Context) {
		resp, err := _this.newService.ListNews(c)
		_this.HandleResponse(c, resp, err)
	}
}

// NewsDetails Get detail of home banner
// @Summary NewsHandler - NewsDetails
// @Tags News
// @Accept json
// @Produce json
// @Param 	newsID 					path 		int 					true 	"News ID"
// @Success 200 {object} dtos.NewsData
// @Failure 400,401,404,500 {object} meta.Error
// @Security BearerAuth
// @Router /v1/news/{newsID} [GET]
func (_this *NewsHandler) NewsDetails() gin.HandlerFunc {
	return func(c *gin.Context) {
		newsID, err := strconv.ParseInt(strings.TrimSpace(c.Param("news-id")), 10, 64)
		if err != nil || newsID < 1 {
			_this.RespondError(c, errors.NewCusErr(errors.ErrCommonInvalidRequest))
			return
		}
		resp, err := _this.newService.NewsDetails(c, newsID)
		_this.HandleResponse(c, resp, err)
	}
}

// GetLatest Get latest news
// @Summary NewsHandler - GetLatest
// @Tags News
// @Accept json
// @Produce json
// @Param 	display_type 	query 	string 	false "Display type, default BANNER" Enums(NEWS,BANNER,VIP_MESSAGE)
// @Success 200 {object} meta.BasicResponse{data=dtos.NewsData}
// @Failure 400,401,404,500 {object} meta.Error
// @Security BearerAuth
// @Router /v1/news/latest [GET]
func (_this *NewsHandler) GetLatest() gin.HandlerFunc {
	return func(c *gin.Context) {
		displayType := c.Query("display_type")
		if displayType == "" {
			displayType = string(models.NewDisplayTypeBanner)
		}
		resp, err := _this.newService.GetLatest(c, displayType)
		_this.HandleResponse(c, resp, err)
	}
	// 3 layer: handler --> services --> repositories
}
