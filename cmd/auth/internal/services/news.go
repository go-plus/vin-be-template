package services

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/dig"
	"vin-be-template/internal/logger"
	"vin-be-template/internal/meta"
	"vin-be-template/internal/repositories"
)

type INewsService interface {
	ListNews(c *gin.Context) (*meta.BasicResponse, error)
	NewsDetails(c *gin.Context, newsID int64) (*meta.BasicResponse, error)
	GetLatest(c *gin.Context, displayType string) (*meta.BasicResponse, error)
}

type newsService struct {
	logger  logger.Logger
	newRepo repositories.INewRepository
}

type NewsServiceArgs struct {
	dig.In
	Logger  logger.Logger
	NewRepo repositories.INewRepository
}

func NewNewsService(params NewsServiceArgs) INewsService {
	return &newsService{
		logger:  params.Logger,
		newRepo: params.NewRepo,
	}
}

func (_this *newsService) ListNews(c *gin.Context) (*meta.BasicResponse, error) {
	return nil, nil
}

func (_this *newsService) NewsDetails(c *gin.Context, newsID int64) (*meta.BasicResponse, error) {
	return nil, nil
}

func (_this *newsService) GetLatest(c *gin.Context, displayType string) (*meta.BasicResponse, error) {
	return nil, nil
}
