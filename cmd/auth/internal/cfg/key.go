package cfg

const (
	ConfigKeyEnvironment = "ENVIRONMENT"
	ConfigKeyContextPath = "CONTEXT_PATH"

	ConfigKeyDBMySQLUsername         = "DB_MYSQL_USERNAME"
	ConfigKeyDBMySQLPassword         = "DB_MYSQL_PASSWORD"
	ConfigKeyDBMySQLHost             = "DB_MYSQL_HOST"
	ConfigKeyDBMySQLPort             = "DB_MYSQL_PORT"
	ConfigKeyDBMySQLDatabase         = "DB_MYSQL_DATABASE"
	ConfigKeyDBMaxIdleConnections    = "DB_MYSQL_MAX_IDLE_CONNECTIONS"
	ConfigKeyDBMaxOpenConnections    = "DB_MYSQL_MAX_OPEN_CONNECTIONS"
	ConfigKeyDBConnectionMaxLifetime = "DB_MYSQL_CONNECTION_MAX_LIFETIME"
	ConfigKeyDBMySQLLogBug           = "DB_MYSQL_LOG_BUG"

	ConfigKeyHttpAddress     = "HTTP_ADDR"
	ConfigKeyHttpPort        = "HTTP_PORT"
	ConfigApiDefaultPageSize = "API_DEFAULT_PAGE_SIZE"
	ConfigApiMinPageSize     = "API_MIN_PAGE_SIZE"
	ConfigApiMaxPageSize     = "API_MAX_PAGE_SIZE"

	JWTSecretKey           = "JWT_SECRET_KEY"
	JWTTokenExpireInMinute = "JWT_TOKEN_EXPIRE_IN_MINUTE"
	JWTPublicKeyFile       = "JWT_PUBLIC_KEY_FILE"
	JwksUrl                = "AUTH0_JWKS_URL"
)
