package dtos

type ListNewsData struct {
	ID               int64  `json:"id"`
	Title            string `json:"title"`
	DisplayType      string `json:"display_type"`
	AnnouncementType string `json:"announcement_type"`
	StartTime        *int64 `json:"start_time"`
	EndTime          *int64 `json:"end_time"`
	CreatedTime      int64  `json:"created_time"`
}

type NewsData struct {
	ID               int64       `json:"id"`
	Title            string      `json:"title"`
	Content          string      `json:"content"`
	DisplayType      string      `json:"display_type"`
	CreatedTime      int64       `json:"created_time"`
	UpdatedTime      *int64      `json:"updated_time"`
	StartTime        *int64      `json:"start_time"`
	EndTime          *int64      `json:"end_time"`
	Image            string      `json:"image"`
	CTATitle         string      `json:"cta_title"`
	DeepLink         string      `json:"deep_link"`
	AnnouncementType string      `json:"announcement_type"`
	ContentImages    interface{} `json:"content_images"`
}

type ContestData struct {
	ID               int64       `json:"id"`
	Title            string      `json:"title"`
	Content          string      `json:"content"`
	DisplayType      string      `json:"display_type"`
	CreatedTime      int64       `json:"created_time"`
	UpdatedTime      *int64      `json:"updated_time"`
	StartTime        *int64      `json:"start_time"`
	EndTime          *int64      `json:"end_time"`
	Image            string      `json:"image"`
	CTATitle         string      `json:"cta_title"`
	DeepLink         string      `json:"deep_link"`
	AnnouncementType string      `json:"announcement_type"`
	ContentImages    interface{} `json:"content_images"`
	RankTable        []RankInfo  `json:"rank_table"`
}

type RankInfo struct {
	Name       string  `json:"name"`
	Value      string  `json:"value"`
	FloatValue float64 `json:"-"`
}

type ConfigProvinceTarget struct {
	CdmID  int64   `json:"cdm_id"`
	Target float64 `json:"target"`
	// Name   string  `json:"name"`
}
