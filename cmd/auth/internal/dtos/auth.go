package dtos

// LoginAuthRequest represents the request of sale login
type LoginAuthRequest struct {
	Username    string `json:"username" validate:"required,max=255,min=3"`
	Password    string `json:"password" validate:"required,max=255,min=3"`
	DeviceToken string
}
type LoginAuthResponse struct {
} // Data Objects
