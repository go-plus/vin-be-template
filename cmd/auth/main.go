package main

import (
	"log"
	"vin-be-template/cmd/auth/internal/providers"
	_ "vin-be-template/docs"
	cfg2 "vin-be-template/internal/cfg"
	"vin-be-template/internal/ginServer"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
)

// @title SalesInternal API
// @version 1.0
// @description This is api document for SalesInternal.
// @termsOfService
// @contact.name API Support
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @license.name This api docs belong to 1ID
// @BasePath /backend
// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization

func main() {
	providers.BuildContainer()
	if viper.GetString("ENVIRONMENT") == cfg2.EnvironmentPro {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}

	log.Println("Preparing and running main application in MODE=" + gin.Mode())
	if err := run(); err != nil {
		log.Fatalf("Running HTTP server: %v", err)
	}
}

func run() error {
	c := providers.GetContainer()
	if c == nil {
		log.Fatalf("Container hasn't been initialized yet")
	}
	var s ginServer.Server
	if err := c.Invoke(func(_s ginServer.Server) { s = _s }); err != nil {
		return err
	}

	if err := s.Open(); err != nil {
		return err
	}

	return nil
}
